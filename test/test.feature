Feature: Temperature Conversion

    Scenario: Modified the mode of temperature to Fahrenheit
        Given Mode of temperature is "Celsius"
        When I modified the mode of temperature to "Fahrenheit"
        Then I can see the text below mode selection changes to "請輸入華氏溫度"

    Scenario: Modified the mode of temperature to Celsius
        Given Mode of temperature is "Fahrenheit"
        When I modified the mode of temperature to "Celsius"
        Then I can see the text below mode selection changes to "請輸入攝氏溫度"

    Scenario: Don't input temperature and clicked convert button
        Given Temperature input field is empty
        When I clicked the convert button
        Then I can see "輸入溫度不可為空白" below the input field

    Scenario Outline: The result had to be displayed corresponded by the mode of temperature selected by I
        Given Mode of temperature is "<mode>"
        When I input <degree> and clcked the conversion button
        Then I can see the "<result>" as a conversion result

        Examples:
        | mode | degree | result |
        | Celsius     | 0  | 轉換結果為華氏32.00度  |
        | Celsius     | 17 | 轉換結果為華氏62.60度  |
        | Celsius     | 39 | 轉換結果為華氏102.20度 |
        | Celsius     | 88 | 轉換結果為華氏190.40度 |
        | Fahrenheit  | -14| 轉換結果為攝氏-25.56度 |
        | Fahrenheit  | 28 | 轉換結果為攝氏-2.22度  |
        | Fahrenheit  | 59 | 轉換結果為攝氏15.00度  |
        | Fahrenheit  | 76 | 轉換結果為攝氏24.44度  |