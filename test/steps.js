const expect = require('expect')
const { Given, When, Then } = require('cucumber')

Given('Mode of temperature is {string}',function (var1) {
    if (var1 === 'Celsius') {
        // let elem = await $("//option[. ='攝氏轉華氏']")
        // await elem.click()
        $("//option[. ='攝氏轉華氏']").click()

        // await this.driver.findElement(By.xpath("//option[. ='攝氏轉華氏']")).click();
    }
    else if (var1 === 'Fahrenheit')
        $("//option[. ='華氏轉攝氏']").click()
        // await this.driver.findElement(By.xpath("//option[. ='華氏轉攝氏']")).click();
})

When('I modified the mode of temperature to {string}', function (var1) {
    if (var1 === 'Celsius')
        $("//option[. ='攝氏轉華氏']").click()
        // await this.driver.findElement(By.xpath("//option[. ='攝氏轉華氏']")).click();
    else if (var1 === 'Fahrenheit')
        $("//option[. ='華氏轉攝氏']").click()
        // await this.driver.findElement(By.xpsssssath("//option[. ='華氏轉攝氏']")).click();
})

Then('I can see the text below mode selection changes to {string}', function (var1) {
    let label = $('#label-temp').getText()
    // let label = await this.driver.findElement(By.id('label-temp')).getText();
    expect(label).toBe(var1);
})


Given('Temperature input field is empty', function () {
    $('#input-temp').setValue('')
    // await this.driver.findElement(By.id('input-temp')).sendKeys('')
})

When('I clicked the convert button', function () {
    $('.btn.btn-primary').click()
    // await this.driver.findElement(By.css('.btn.btn-primary')).click();
})

Then('I can see {string} below the input field', function (var1) {
    let errmsg = $('#error-msg').getText()
    // let errmsg = await this.driver.findElement(By.id('error-msg')).getText();
    expect(errmsg).toBe(var1)
})


When('I input {int} and clcked the conversion button', function (var1) {
    $('#input-temp').setValue(var1)
    // await this.driver.findElement(By.id('input-temp')).sendKeys(var1);
    $('.btn.btn-primary').click()
    // await this.driver.findElement(By.css('.btn.btn-primary')).click();
})

Then('I can see the {string} as a conversion result', function (var1) {
    let result = $('h3').getText()
    // let result = await this.driver.findElement(By.tagName('h3')).getText();
    expect(result).toBe(var1);
})
